package com.getjavajob.training.web1702.volkovn.project.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;

@Entity
@Table(name = "phones")
@XStreamAlias("phone")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String country;
    private String area;
    private String number;
    @Enumerated(EnumType.STRING)
    private PhoneType type;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "acc_id")
    @XStreamOmitField
    private Account account;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phone phone = (Phone) o;
        if (country != null ? !country.equals(phone.country) : phone.country != null) {
            return false;
        }
        if (area != null ? !area.equals(phone.area) : phone.area != null) {
            return false;
        }
        return number != null ? number.equals(phone.number) : phone.number == null;
    }

    @Override
    public int hashCode() {
        int result = country != null ? country.hashCode() : 0;
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "country='" + country + '\'' +
                ", area='" + area + '\'' +
                ", number='" + number + '\'' +
                ", type=" + type +
                '}';
    }
}
