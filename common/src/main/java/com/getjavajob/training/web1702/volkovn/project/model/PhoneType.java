package com.getjavajob.training.web1702.volkovn.project.model;

public enum PhoneType {
    PRV("Private"), WRK("Work");

    private String fullType;

    PhoneType(String fullType) {
        this.fullType = fullType;
    }

    public String getFullType() {
        return fullType;
    }

    public static PhoneType getByFullType(String fullType) {
        for (PhoneType type : PhoneType.values()) {
            if (type.getFullType().equals(fullType)) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }
}
