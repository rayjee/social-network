package com.getjavajob.training.web1702.volkovn.project.model;

import java.io.Serializable;

public class GroupMembershipPK implements Serializable {
    private Account account;
    private Group group;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
