package com.getjavajob.training.web1702.volkovn.project.model;

public interface Picture {

    byte[] getPicture();

    void setPicture(byte[] picture);

}
