package com.getjavajob.training.web1702.volkovn.project.util;

public class MembershipStatusBean {
    private static final MembershipStatus NONE = MembershipStatus.NONE;
    private static final MembershipStatus REQUEST = MembershipStatus.REQUEST;
    private static final MembershipStatus MEMBER = MembershipStatus.MEMBER;
    private static final MembershipStatus MODERATOR = MembershipStatus.MODERATOR;

    private MembershipStatus status;

    public void setStatus(MembershipStatus status) {
        this.status = status;
    }

    public MembershipStatus getStatus() {
        return status;
    }

    public MembershipStatus getNone() {
        return NONE;
    }

    public MembershipStatus getRequest() {
        return REQUEST;
    }

    public MembershipStatus getMember() {
        return MEMBER;
    }

    public MembershipStatus getModerator() {
        return MODERATOR;
    }
}
