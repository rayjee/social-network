package com.getjavajob.training.web1702.volkovn.project.model;

import javax.persistence.*;
import javax.persistence.Id;

@Entity
@Table(name = "group_members")
@IdClass(GroupMembershipPK.class)
public class GroupMembership {
    @Id
    @ManyToOne
    @JoinColumn(name = "acc_id")
    private Account account;
    @Id
    @ManyToOne
    @JoinColumn(name = "gr_id")
    private Group group;
    private int status;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupMembership that = (GroupMembership) o;
        return status == that.status && account.equals(that.account) && group.equals(that.group);
    }

    @Override
    public int hashCode() {
        int result = account.hashCode();
        result = 31 * result + group.hashCode();
        result = 31 * result + status;
        return result;
    }
}
