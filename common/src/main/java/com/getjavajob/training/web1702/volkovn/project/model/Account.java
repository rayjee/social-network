package com.getjavajob.training.web1702.volkovn.project.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "accounts")
public class Account implements Picture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    private String patronymic;
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate birthday;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "account")
    private List<Phone> phones;
    @Column(name = "home_address")
    private String homeAddress;
    @Column(name = "work_address")
    private String workAddress;
    @Column(nullable = false, unique = true)
    private String email;
    private String icq;
    private String skype;
    private String extra;
    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "friendships",
            joinColumns = @JoinColumn(name = "acc_id_2", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "acc_id_1", referencedColumnName = "id", nullable = false))
    private List<Account> friends;
    @ManyToMany(mappedBy = "friends", cascade = CascadeType.MERGE)
    private List<Account> friendOf;
    @Column(name = "avatar")
    private byte[] picture;
    private String password;
    @Column(name = "reg_date")
    private LocalDate regDate;
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<GroupMembership> memberships;

    public Account() {
        phones = new ArrayList<>();
        memberships = new ArrayList<>();
        friends = new ArrayList<>();
        friendOf = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void addPhones(List<Phone> phones) {
        this.phones.addAll(phones);
    }

    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public void removePhones() {
        this.phones.clear();
    }

    public void removePhones(List<Phone> phones) {
        this.phones.removeAll(phones);
    }

    public void removePhone(Phone phone) {
        phones.remove(phone);
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public byte[] getPicture() {
        return picture;
    }

    @Override
    public void setPicture(byte[] avatar) {
        this.picture = avatar;
    }

    public LocalDate getRegDate() {
        return regDate;
    }

    public void setRegDate(LocalDate regDate) {
        this.regDate = regDate;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    public List<Account> getFriendOf() {
        return friendOf;
    }

    public void setFriendOf(List<Account> friendOf) {
        this.friendOf = friendOf;
    }

    public void setMemberships(List<GroupMembership> memberships) {
        this.memberships = memberships;
    }

    public List<GroupMembership> getMemberships() {
        return memberships;
    }

    public void addFriend(Account friend) {
        friends.add(friend);
    }

    public void addFriendOf(Account account) {
        friendOf.add(account);
    }

    public void removeFriend(Account friend) {
        friends.remove(friend);
    }

    public void removeFriendOf(Account account) {
        friendOf.remove(account);
    }

    public void addGroupMembership(GroupMembership groupMembership) {
        memberships.add(groupMembership);
    }

    public List<GroupMembership> getMembershipsExceptRequests() {
        List<GroupMembership> result = new ArrayList<>(memberships);
        result.removeIf(membership -> membership.getStatus() == 1);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return email != null ? email.equals(account.email) : account.email == null;
    }

    @Override
    public int hashCode() {
        return email != null ? email.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthday=" + birthday +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", extra='" + extra + '\'' +
                '}';
    }
}
