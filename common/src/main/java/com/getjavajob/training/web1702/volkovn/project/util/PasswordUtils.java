package com.getjavajob.training.web1702.volkovn.project.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class PasswordUtils {

    private static final int PBKDF2_ITERATIONS = 1000;
    private static final int HASH_BYTES = 24;
    private static final int SALT_BYTES = 16;

    public static String generatePasswordHash(String password) throws PasswordException {
        char[] chars = password.toCharArray();
        byte[] salt;
        try {
            salt = getSalt();
        } catch (NoSuchAlgorithmException e) {
            throw new PasswordException(e);
        }

        PBEKeySpec spec = new PBEKeySpec(chars, salt, PBKDF2_ITERATIONS, HASH_BYTES);
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return toHex(salt) + ":" + toHex(hash);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new PasswordException(e);
        }
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[SALT_BYTES];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    public static boolean validatePassword(String originalPassword, String storedPassword) throws PasswordException {
        String[] parts = storedPassword.split(":");
        if (parts.length < 2) {
            throw new IllegalStateException("Invalid password");
        }
        byte[] salt;
        byte[] hash;
        try {
            salt = fromHex(parts[0]);
            hash = fromHex(parts[1]);
        } catch (NoSuchAlgorithmException e) {
            throw new PasswordException(e);
        }
        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, PBKDF2_ITERATIONS, HASH_BYTES);
        SecretKeyFactory skf;
        byte[] testHash;
        try {
            skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            testHash = skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new PasswordException(e);
        }

        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    private static byte[] fromHex(String hex) throws NoSuchAlgorithmException {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
