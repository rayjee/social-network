package com.getjavajob.training.web1702.volkovn.project.util;

public class PasswordException extends Exception {
    public PasswordException(Throwable cause) {
        super(cause);
    }
}
