package com.getjavajob.training.web1702.volkovn.project.util;

public enum MembershipStatus {
    NONE, REQUEST, MEMBER, MODERATOR;

    public static MembershipStatus getStatus(int status) {
        switch (status) {
            case 1 : return REQUEST;
            case 2 : return MEMBER;
            case 3 : return MODERATOR;
            default: return NONE;
        }
    }
}
