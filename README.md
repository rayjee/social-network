# Yet Another Social Network  
  
** Functionality: **  
  
+ registration  
+ ajax autocomplete loading  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ upload and download avatar  
+ users export to xml  
+ groups  
  
** Tools: **  
JDK 8, Spring 4, JPA 2 / Hibernate 5, XStream, jQuery 2, JUnit 4, Mockito, log4j2, JSP, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 17.  
  
** Notes: **  
SQL ddl is located in the `init.sql`  
  
** Screenshots: **  
https://cloud.mail.ru/public/Dm4C/LHwszQkyk  
https://cloud.mail.ru/public/9Qpg/83uFEYMfh  
https://cloud.mail.ru/public/3w93/EK1EV3PKm  
https://cloud.mail.ru/public/KFYQ/Ft7fb8DLC  
  
** App: **  
https://young-thicket-60088.herokuapp.com/  
login: 3@mail.com, password: 3  
  
--  
**Volkov Nikolay**  
Training getJavaJob,  
http://www.getjavajob.com  