package com.getjavajob.training.web1702.volkovn.project.service;

import com.getjavajob.training.web1702.volkovn.project.dao.AccountDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupMembershipDAO;
import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    @Mock
    private GroupDAO groupDAO;
    @Mock
    private GroupMembershipDAO groupMembershipDAO;
    @Mock
    private AccountDAO accountDAO;
    @Mock
    private Group group1;
    @Mock
    private Group group2;
    @Mock
    private Account account;
    @InjectMocks
    private GroupService groupService = new GroupService(groupDAO, groupMembershipDAO, accountDAO);

    @Test
    public void testGetByName() {
        String name = "name";
        when(groupDAO.getByName(name, 0, 10)).thenReturn(asList(group1, group2));
        assertEquals(asList(group1, group2), groupService.getByName(name, 0, 10));
    }

    @Test
    public void testGet() {
        when(groupDAO.get((long) 1)).thenReturn(group1);
        assertEquals(group1, groupService.get((long) 1));
    }

    @Test
    public void testCreate() {
        when(accountDAO.get(account.getId())).thenReturn(account);
        groupService.create(group1, account);
        verify(groupDAO).create(group1);
        verify(groupMembershipDAO).insertModerator(group1, account);
    }

    @Test
    public void testDelete() {
        groupService.delete(group1.getId());
        verify(groupDAO).delete(group1.getId());
    }

    @Test
    public void testUpdate() {
        groupService.update(group1);
        verify(groupDAO).update(group1);
    }

    @Test
    public void testGetAll() {
        when(groupDAO.getAll()).thenReturn(asList(group1, group2));
        assertEquals(asList(group1, group2), groupService.getAll());
    }
}
