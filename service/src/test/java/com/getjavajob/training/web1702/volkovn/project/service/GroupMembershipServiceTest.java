package com.getjavajob.training.web1702.volkovn.project.service;

import com.getjavajob.training.web1702.volkovn.project.dao.AccountDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupMembershipDAO;
import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.getjavajob.training.web1702.volkovn.project.util.MembershipStatus.REQUEST;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupMembershipServiceTest {
    @Mock
    private GroupMembershipDAO groupMembershipDAO;
    @Mock
    private AccountDAO accountDAO;
    @Mock
    private GroupDAO groupDAO;
    @Mock
    private Account account;
    @Mock
    private Group group;
    @InjectMocks
    private GroupMembershipService groupMembershipService =
            new GroupMembershipService(groupMembershipDAO, accountDAO, groupDAO);

    @Test
    public void testJoinGroup() {
        when(accountDAO.get(account.getId())).thenReturn(account);
        when(groupDAO.get(group.getId())).thenReturn(group);
        groupMembershipService.joinGroup(group.getId(), account.getId());
        verify(groupMembershipDAO).joinGroup(group, account);
    }

    @Test
    public void testLeaveGroup() {
        when(accountDAO.get(account.getId())).thenReturn(account);
        when(groupDAO.get(group.getId())).thenReturn(group);
        groupMembershipService.leaveGroup(group.getId(), account.getId());
        verify(groupMembershipDAO).leaveGroup(group, account);
    }

    @Test
    public void testGetMembershipStatus() {
        when(groupMembershipDAO.getMembershipStatus(group, account)).thenReturn(1);
        assertEquals(REQUEST, groupMembershipService.getMembershipStatus(group, account));
    }

    @Test
    public void testInsertModerator() {
        when(accountDAO.get(account.getId())).thenReturn(account);
        when(groupDAO.get(group.getId())).thenReturn(group);
        groupMembershipService.insertModerator(group.getId(), account.getId());
        verify(groupMembershipDAO).insertModerator(group, account);
    }

    @Test
    public void testApproveMembershipRequest() {
        when(accountDAO.get(account.getId())).thenReturn(account);
        when(groupDAO.get(group.getId())).thenReturn(group);
        groupMembershipService.approveMembershipRequest(group.getId(), account.getId());
        verify(groupMembershipDAO).approveMembershipRequest(group, account);
    }

    @Test
    public void testSetModerator() {
        when(accountDAO.get(account.getId())).thenReturn(account);
        when(groupDAO.get(group.getId())).thenReturn(group);
        groupMembershipService.setModerator(group.getId(), account.getId());
        verify(groupMembershipDAO).setModerator(group, account);
    }
}
