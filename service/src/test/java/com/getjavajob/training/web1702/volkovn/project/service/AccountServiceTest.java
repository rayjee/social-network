package com.getjavajob.training.web1702.volkovn.project.service;

import com.getjavajob.training.web1702.volkovn.project.dao.AccountDAO;
import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.util.PasswordException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.getjavajob.training.web1702.volkovn.project.util.PasswordUtils.generatePasswordHash;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @Mock
    private AccountDAO accountDAO;
    @Mock
    private Account account1;
    @Mock
    private Account account2;
    @Mock
    private Account account3;
    @Mock
    private Group group;
    @InjectMocks
    private AccountService accountService = new AccountService(accountDAO);

    @Test
    public void testGetFriends() {
        when(accountDAO.getFriends(account1.getId())).thenReturn(asList(account2, account3));
        assertEquals(asList(account2, account3), accountService.getFriends(account1));
    }

    @Test
    public void testValidateAccount() throws PasswordException, ServiceException {
        String email = "e@mail";
        String password = "123";
        when(accountDAO.getByEmail(email)).thenReturn(account1);
        when(account1.getPassword()).thenReturn(generatePasswordHash(password));
        assertEquals(account1, accountService.validateAccount(email, password));
    }

    @Test (expected = ServiceException.class)
    public void testValidateAccountInvalidPassword() throws ServiceException, PasswordException {
        String email = "e@mail";
        when(accountDAO.getByEmail(email)).thenReturn(account1);
        when(account1.getPassword()).thenReturn(generatePasswordHash("123"));
        assertEquals(account1, accountService.validateAccount(email, "321"));
    }

    @Test
    public void testGetByEmailAndHash() {
        String email = "e@mail";
        String hash = "123";
        when(accountDAO.getByEmailAndHash(email, hash)).thenReturn(account1);
        assertEquals(account1, accountService.getByEmailAndHash(email, hash));
    }

    @Test
    public void testGetByName() {
        String name = "name";
        when(accountDAO.getByName(name, 0, 10)).thenReturn(asList(account1, account2));
        assertEquals(asList(account1, account2), accountService.getByName(name, 0 ,10));
    }

    @Test
    public void testGetGroupRequests() {
        when(accountDAO.getGroupRequests(group)).thenReturn(asList(account1, account2));
        assertEquals(asList(account1, account2), accountService.getGroupRequests(group));
    }

    @Test
    public void testGet() {
        when(accountDAO.get((long) 1)).thenReturn(account1);
        assertEquals(account1, accountService.get((long) 1));
    }

    @Test
    public void testCreate() {
        when(accountDAO.create(account1)).thenReturn(account2);
        assertEquals(account2, accountService.create(account1));
    }

    @Test
    public void testUpdate() {
        accountService.update(account1);
        verify(accountDAO).update(account1);
    }

    @Test
    public void testGetAll() {
        when(accountDAO.getAll()).thenReturn(asList(account1, account2));
        assertEquals(asList(account1, account2), accountService.getAll());
    }

    @Test
    public void testDelete() {
        accountService.delete(account1.getId());
        verify(accountDAO).delete(account1.getId());
    }

    @Test
    public void testAddFriend() {
        accountService.addFriend(account1, account2);
        verify(accountDAO).insertFriendship(account1, account2);
    }

    @Test
    public void testRemoveFriend() {
        accountService.removeFriend(account1, account2);
        verify(accountDAO).deleteFriendship(account1, account2);
    }
}
