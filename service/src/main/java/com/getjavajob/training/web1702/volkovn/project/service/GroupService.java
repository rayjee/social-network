package com.getjavajob.training.web1702.volkovn.project.service;

import com.getjavajob.training.web1702.volkovn.project.dao.AccountDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupMembershipDAO;
import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembership;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@org.springframework.stereotype.Service
public class GroupService implements Service<Group> {
    private GroupDAO groupDAO;
    private GroupMembershipDAO groupMembershipDAO;
    private AccountDAO accountDAO;

    @Autowired
    public GroupService(GroupDAO groupDAO, GroupMembershipDAO groupMembershipDAO, AccountDAO accountDAO) {
        this.groupDAO = groupDAO;
        this.groupMembershipDAO = groupMembershipDAO;
        this.accountDAO = accountDAO;
    }

    @Transactional
    public Group create(Group group, Account account) {
        groupDAO.create(group);
        GroupMembership groupMembership = groupMembershipDAO.insertModerator(group, accountDAO.get(account.getId()));
        group.addGroupMembership(groupMembership);
        return group;
    }

    @Override
    public Group get(long id) {
        return groupDAO.get(id);
    }

    @Transactional(readOnly = true)
    public Group getFullyInitialized(long id) {
        Group group = groupDAO.get(id);
        Hibernate.initialize(group.getMemberships());
        Hibernate.initialize(group.getCreator());
        return group;
    }

    @Override
    @Transactional
    public Group update(Group group) {
        return groupDAO.update(group);
    }

    @Override
    @Transactional
    public void delete(long id) {
        groupDAO.delete(id);
    }

    @Override
    public List<Group> getAll() {
        return groupDAO.getAll();
    }

    @Override
    @Transactional
    public Group create(Group group) {
        Group created;
        created = groupDAO.create(group);
        return created;
    }

    public List<Group> getByName(String name, int offset, int limit) {
        return groupDAO.getByName(name, offset, limit);
    }

    public long getCountByName(String name) {
        return groupDAO.getCountByName(name);
    }
}
