package com.getjavajob.training.web1702.volkovn.project.service;

import com.getjavajob.training.web1702.volkovn.project.dao.AccountDAO;
import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.service.xml.AccountDTO;
import com.getjavajob.training.web1702.volkovn.project.service.xml.XSDValidator;
import com.getjavajob.training.web1702.volkovn.project.util.PasswordException;
import com.thoughtworks.xstream.XStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static com.getjavajob.training.web1702.volkovn.project.util.PasswordUtils.validatePassword;
import static org.springframework.util.StringUtils.capitalize;

@org.springframework.stereotype.Service
public class AccountService implements Service<Account> {
    public static final String INVALID_PASSWORD_MESSAGE = "Invalid password";
    private static final String ACCOUNT_XSD = "account.xsd";

    private AccountDAO accountDAO;

    @Autowired
    public AccountService(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    @Transactional
    public Account create(Account account) {
        return accountDAO.create(account);
    }

    @Override
    @Transactional
    public Account update(Account account) {
        return accountDAO.update(account);
    }

    @Override
    @Transactional
    public void delete(long id) {
        accountDAO.delete(id);
    }

    @Override
    public List<Account> getAll() {
        return accountDAO.getAll();
    }

    @Override
    public Account get(long id) {
        return accountDAO.get(id);
    }

    @Transactional(readOnly = true)
    public Account getWithPhones(long id) {
        return accountDAO.getWithPhones(id);
    }

    @Transactional(readOnly = true)
    public Account getFullyInitialized(long id) {
        return accountDAO.getFullyInitialized(id);
    }

    public List<Account> getFriends(Account account) {
        return accountDAO.getFriends(account.getId());
    }

    public Account validateAccount(String email, String password) throws ServiceException {
        Account account;
        account = accountDAO.getByEmail(email);
        try {
            if (account != null && validatePassword(password, account.getPassword())) {
                return account;
            } else {
                throw new ServiceException(INVALID_PASSWORD_MESSAGE);
            }
        } catch (PasswordException | DataAccessException | IllegalStateException e) {
            throw new ServiceException(e);
        }
    }

    public Account getByEmailAndHash(String email, String hash) {
        return accountDAO.getByEmailAndHash(email, hash);
    }

    public List<Account> getByName(String name, int offset, int limit) {
        return accountDAO.getByName(name, offset, limit);
    }

    public long getCountByName(String name) {
        return accountDAO.getCountByName(name);
    }

    public List<Account> getGroupRequests(Group group) {
        return accountDAO.getGroupRequests(group);
    }

    @Transactional
    public void addFriend(Account account, Account friend) {
        accountDAO.insertFriendship(account, friend);
    }

    @Transactional
    public void removeFriend(Account account, Account friend) {
        accountDAO.deleteFriendship(account, friend);
    }

    @Transactional(readOnly = true)
    public String toXml(long id) throws ServiceException {
        XStream xStream = new XStream();
        xStream.processAnnotations(AccountDTO.class);
        Account account = accountDAO.getWithPhones(id);
        String xml;
        try {
            xml = xStream.toXML(toDTO(account));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new ServiceException(e);
        }
        return xml;
    }

    private AccountDTO toDTO(Account account)
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        AccountDTO dto = new AccountDTO();
        Field[] fields = dto.getClass().getDeclaredFields();
        Class<?> accountClass = account.getClass();
        for (Field field : fields) {
            String fieldName = field.getName();
            if (!"phones".equals(fieldName)) {
                Object accountFieldValue = accountClass
                        .getDeclaredMethod("get" + capitalize(fieldName))
                        .invoke(account);
                field.setAccessible(true);
                field.set(dto, accountFieldValue);
            }
        }
        account.getPhones().forEach(phone -> {
            dto.getPhones().add(phone);
        });
        return dto;
    }

    @Transactional
    public void fromXml(long id, byte[] xml) throws ServiceException {
        validateXml(xml);
        XStream xStream = new XStream();
        xStream.processAnnotations(AccountDTO.class);
        AccountDTO dto = (AccountDTO) xStream.fromXML(new ByteArrayInputStream(xml));
        Account account;
        try {
            account = fromDTO(dto, id);
        } catch (NoSuchMethodException | InvocationTargetException | NoSuchFieldException | IllegalAccessException e) {
            throw new ServiceException(e);
        }
        accountDAO.update(account);
    }

    private void validateXml(byte[] xml) throws ServiceException {
        try (InputStream xmlStream = new ByteArrayInputStream(xml);
             InputStream xsdStream = this.getClass().getClassLoader().getResourceAsStream(ACCOUNT_XSD)) {
            XSDValidator validator = new XSDValidator();
            validator.validate(xsdStream, xmlStream);
        } catch (IOException | SAXException e) {
            throw new ServiceException(e);
        }
    }

    private Account fromDTO(AccountDTO dto, long id)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Account account = new Account();
        Field[] fields = dto.getClass().getDeclaredFields();
        Class<?> dtoClass = dto.getClass();
        Class<?> accountClass = account.getClass();
        for (Field field : fields) {
            String fieldName = field.getName();
            Object dtoFieldValue = dtoClass
                    .getDeclaredMethod("get" + capitalize(fieldName))
                    .invoke(dto);
            Field accountField = accountClass.getDeclaredField(fieldName);
            accountField.setAccessible(true);
            accountField.set(account, dtoFieldValue);
        }
        setNotSerializedFields(id, account);
        return account;
    }

    @Transactional(readOnly = true)
    private void setNotSerializedFields(long id, Account account) {
        account.setId(id);
        account.getPhones().forEach(phone -> phone.setAccount(account));
        Account oldAccount = accountDAO.getFullyInitialized(id);
        account.setFriends(oldAccount.getFriends());
        account.setFriendOf(oldAccount.getFriendOf());
        account.setMemberships(oldAccount.getMemberships());
        account.setPassword(oldAccount.getPassword());
    }
}
