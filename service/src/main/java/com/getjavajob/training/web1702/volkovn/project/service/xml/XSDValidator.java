package com.getjavajob.training.web1702.volkovn.project.service.xml;

import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class XSDValidator {
    public void validate(InputStream xsd, InputStream xml) throws SAXException, IOException {
        List<Source> schemas = new ArrayList<>();
        schemas.add(new StreamSource(xsd));
        Source xmlFile = new StreamSource(xml);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        Source[] schemaSources = new Source[schemas.size()];
        schemas.toArray(schemaSources);
        Schema schema = schemaFactory.newSchema(schemaSources);
        Validator validator = schema.newValidator();
        validator.validate(xmlFile);
    }
}
