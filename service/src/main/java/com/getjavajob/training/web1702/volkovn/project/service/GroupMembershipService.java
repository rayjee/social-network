package com.getjavajob.training.web1702.volkovn.project.service;

import com.getjavajob.training.web1702.volkovn.project.dao.AccountDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupDAO;
import com.getjavajob.training.web1702.volkovn.project.dao.GroupMembershipDAO;
import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.util.MembershipStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@org.springframework.stereotype.Service
public class GroupMembershipService {
    private GroupMembershipDAO groupMembershipDAO;
    private AccountDAO accountDAO;
    private GroupDAO groupDAO;

    @Autowired
    public GroupMembershipService(GroupMembershipDAO groupMembershipDAO, AccountDAO accountDAO, GroupDAO groupDAO) {
        this.groupMembershipDAO = groupMembershipDAO;
        this.accountDAO = accountDAO;
        this.groupDAO = groupDAO;
    }

    @Transactional
    public void joinGroup(long grId, long accId) {
        groupMembershipDAO.joinGroup(groupDAO.get(grId), accountDAO.get(accId));
    }

    @Transactional
    public void leaveGroup(long grId, long accId) {
        groupMembershipDAO.leaveGroup(groupDAO.get(grId), accountDAO.get(accId));
    }

    public MembershipStatus getMembershipStatus(Group group, Account account) {
        int status = groupMembershipDAO.getMembershipStatus(group, account);
        return MembershipStatus.getStatus(status);
    }

    @Transactional
    public void insertModerator(long grId, long accId) {
        groupMembershipDAO.insertModerator(groupDAO.get(grId), accountDAO.get(accId));
    }

    @Transactional
    public void approveMembershipRequest(long grId, long accId) {
        groupMembershipDAO.approveMembershipRequest(groupDAO.get(grId), accountDAO.get(accId));
    }

    @Transactional
    public void setModerator(long grId, long accId) {
        groupMembershipDAO.setModerator(groupDAO.get(grId), accountDAO.get(accId));
    }
}
