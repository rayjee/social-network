package com.getjavajob.training.web1702.volkovn.project.service;

import java.util.List;

public interface Service<T> {

    T create(T o);

    T get(long id);

    T update(T o);

    void delete(long id);

    List<T> getAll();

}
