package com.getjavajob.training.web1702.volkovn.project.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.List;

public class GenericDAO<T> implements DAO<T> {

    @PersistenceContext
    protected EntityManager entityManager;
    protected Class<T> typeParameterClass;

    @Override
    public T create(T o) {
        entityManager.persist(o);
        return o;
    }

    @Override
    public T get(Object primaryKey) {
        return entityManager.find(typeParameterClass, primaryKey);
    }

    @Override
    public T update(T o) {
        return entityManager.merge(o);
    }

    @Override
    public void delete(Object primaryKey) {
        T o = entityManager.find(typeParameterClass, primaryKey);
        entityManager.remove(o);
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> from = criteriaQuery.from(typeParameterClass);
        CriteriaQuery<T> select = criteriaQuery.select(from);
        return entityManager.createQuery(select).getResultList();
    }

}
