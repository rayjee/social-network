package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembership;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class GroupDAO extends GenericDAO<Group> {

    public GroupDAO() {
        typeParameterClass = Group.class;
    }

    public List<Group> getGroupsByAccount(Account account) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
        Root<Group> groupRoot = criteriaQuery.from(Group.class);
        Join<GroupMembership, Group> memberships = groupRoot.join("memberships");
        Predicate grIdPredicate = criteriaBuilder.equal(memberships.get("account"), account);
        Predicate status = criteriaBuilder.greaterThan(memberships.get("status"), 1);
        CriteriaQuery<Group> select = criteriaQuery.select(groupRoot).where(grIdPredicate, status);
        return entityManager.createQuery(select).getResultList();
    }

    public List<Group> getByName(String name, int offset, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
        Root<Group> root = criteriaQuery.from(Group.class);
        CriteriaQuery<Group> select = criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), name));
        return entityManager
                .createQuery(select)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    public long getCountByName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
        Root<Group> root = criteriaQuery.from(Group.class);
        CriteriaQuery<Group> select = criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), name));
        return entityManager.createQuery(select).getResultList().size();
    }

}
