package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Phone;
import org.springframework.stereotype.Repository;

@Repository
public class PhoneDAO extends GenericDAO<Phone> {

    public PhoneDAO() {
        typeParameterClass = Phone.class;
    }

}
