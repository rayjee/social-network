package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembership;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class AccountDAO extends GenericDAO<Account> {

    public AccountDAO() {
        typeParameterClass = Account.class;
    }

    public Account getWithPhones(long id) {
        Account account = get(id);
        account.getPhones().forEach(phone -> Hibernate.initialize(phone.getAccount()));
        return account;
    }

    public Account getFullyInitialized(long id) {
        Account account = get(id);
        Hibernate.initialize(account.getPhones());
        Hibernate.initialize(account.getMemberships());
        Hibernate.initialize(account.getFriends());
        Hibernate.initialize(account.getFriendOf());
        return account;
    }

    public List<Account> getFriends(long accId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> accountRoot = criteriaQuery.from(Account.class);
        Join<Account, Account> friends = accountRoot.join("friends");
        Predicate accIdPredicate = criteriaBuilder.equal(accountRoot.get("id"), accId);
        CriteriaQuery<Account> select = criteriaQuery.select(friends).where(accIdPredicate);
        return entityManager.createQuery(select).getResultList();
    }

    public List<Account> getGroupMembers(Group group) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> accountRoot = criteriaQuery.from(Account.class);
        Join<GroupMembership, Account> memberships = accountRoot.join("memberships");
        Predicate grIdPredicate = criteriaBuilder.equal(memberships.get("group"), group);
        Predicate status = criteriaBuilder.greaterThan(memberships.get("status"), 1);
        CriteriaQuery<Account> select = criteriaQuery.select(accountRoot).where(grIdPredicate, status);
        return entityManager.createQuery(select).getResultList();
    }

    public List<Account> getGroupRequests(Group group) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> accountRoot = criteriaQuery.from(Account.class);
        Join<GroupMembership, Account> memberships = accountRoot.join("memberships");
        Predicate grIdPredicate = criteriaBuilder.equal(memberships.get("group"), group);
        Predicate status = criteriaBuilder.equal(memberships.get("status"), 1);
        CriteriaQuery<Account> select = criteriaQuery.select(accountRoot).where(grIdPredicate, status);
        return entityManager.createQuery(select).getResultList();
    }

    public Account getByEmail(String email) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate emailPredicate = criteriaBuilder.equal(root.get("email"), email);
        CriteriaQuery<Account> select = criteriaQuery.select(root).where(emailPredicate);
        return entityManager.createQuery(select).getSingleResult();
    }

    public Account getByEmailAndHash(String email, String hash) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate emailPredicate = criteriaBuilder.equal(root.get("email"), email);
        Predicate hashPredicate = criteriaBuilder.equal(root.get("password"), hash);
        CriteriaQuery<Account> select = criteriaQuery.select(root).where(emailPredicate, hashPredicate);
        return entityManager.createQuery(select).getSingleResult();
    }

    public List<Account> getByName(String name, int offset, int limit) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate firstName = criteriaBuilder.like(root.get("firstName"), name);
        Predicate lastName = criteriaBuilder.like(root.get("lastName"), name);
        CriteriaQuery<Account> select = criteriaQuery.select(root).where(criteriaBuilder.or(firstName, lastName));
        return entityManager
                .createQuery(select)
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    public long getCountByName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
        Root<Account> root = criteriaQuery.from(Account.class);
        Predicate firstName = criteriaBuilder.like(root.get("firstName"), name);
        Predicate lastName = criteriaBuilder.like(root.get("lastName"), name);
        CriteriaQuery<Account> select = criteriaQuery.select(root).where(criteriaBuilder.or(firstName, lastName));
        return entityManager.createQuery(select).getResultList().size();
    }

    public void insertFriendship(Account account, Account friend) {
        account.addFriend(friend);
        entityManager.merge(account);
        entityManager.flush();
        friend.addFriendOf(account);
    }

    public void deleteFriendship(Account account, Account friend) {
        account.removeFriend(friend);
        friend.removeFriendOf(account);
        entityManager.merge(account);
        entityManager.flush();
    }

}
