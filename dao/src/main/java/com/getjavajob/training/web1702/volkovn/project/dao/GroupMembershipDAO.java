package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembership;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembershipPK;
import org.springframework.stereotype.Repository;

@Repository
public class GroupMembershipDAO extends GenericDAO<GroupMembership> {

    public GroupMembershipDAO() {
        typeParameterClass = GroupMembership.class;
    }

    public GroupMembership insertModerator(Group group, Account account) {
        GroupMembership groupMembership = new GroupMembership();
        groupMembership.setAccount(account);
        groupMembership.setGroup(group);
        groupMembership.setStatus(3);
        return create(groupMembership);
    }

    public void joinGroup(Group group, Account account) {
        GroupMembership groupMembership = new GroupMembership();
        groupMembership.setAccount(account);
        groupMembership.setGroup(group);
        groupMembership.setStatus(1);
        create(groupMembership);
    }

    public void leaveGroup(Group group, Account account) {
        GroupMembershipPK pk = new GroupMembershipPK();
        pk.setAccount(account);
        pk.setGroup(group);
        delete(pk);
    }

    public int getMembershipStatus(Group group, Account account) {
        GroupMembershipPK pk = new GroupMembershipPK();
        pk.setAccount(account);
        pk.setGroup(group);
        GroupMembership groupMembership = get(pk);
        return groupMembership == null ? 0 : groupMembership.getStatus();
    }

    public void approveMembershipRequest(Group group, Account account) {
        GroupMembershipPK pk = new GroupMembershipPK();
        pk.setAccount(account);
        pk.setGroup(group);
        GroupMembership groupMembership = get(pk);
        groupMembership.setStatus(2);
        update(groupMembership);
    }

    public void setModerator(Group group, Account account) {
        GroupMembershipPK pk = new GroupMembershipPK();
        pk.setAccount(account);
        pk.setGroup(group);
        GroupMembership groupMembership = get(pk);
        groupMembership.setStatus(3);
        update(groupMembership);
    }

}
