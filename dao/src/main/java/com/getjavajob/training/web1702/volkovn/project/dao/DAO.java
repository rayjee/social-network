package com.getjavajob.training.web1702.volkovn.project.dao;

import java.util.List;

public interface DAO<T> {

    T create(T o);

    T get(Object primaryKey);

    T update(T o);

    void delete(Object primaryKey);

    List<T> getAll();

}
