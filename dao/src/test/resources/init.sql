CREATE TABLE accounts (
    id SERIAL PRIMARY KEY ,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    patronymic VARCHAR(30),
    birthday DATE,
    home_address VARCHAR(100),
    work_address VARCHAR(100),
    email VARCHAR(50) NOT NULL,
    icq CHAR(9),
    skype VARCHAR(30),
    extra TEXT,
    avatar BLOB,
    password VARCHAR(200) NOT NULL,
    reg_date DATE);

CREATE TABLE phones (
    id SERIAL PRIMARY KEY,
    country VARCHAR(3),
    area VARCHAR(4),
    number VARCHAR(13),
    type CHAR(3),
    acc_id BIGINT UNSIGNED REFERENCES accounts(id) ON DELETE CASCADE);

CREATE TABLE groups (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    description TEXT,
    creation_date DATE,
    creator BIGINT UNSIGNED REFERENCES accounts(id) ON DELETE SET NULL ,
    picture BLOB);

CREATE TABLE group_members (
    gr_id BIGINT UNSIGNED NOT NULL REFERENCES groups(id) ON DELETE CASCADE,
    acc_id BIGINT UNSIGNED NOT NULL REFERENCES accounts(id) ON DELETE CASCADE,
    status TINYINT,
    PRIMARY KEY (gr_id, acc_id));

CREATE TABLE friendships (
    acc_id_1 BIGINT UNSIGNED NOT NULL REFERENCES accounts(id) ON DELETE CASCADE,
    acc_id_2 BIGINT UNSIGNED NOT NULL REFERENCES accounts(id) ON DELETE CASCADE,
    PRIMARY KEY (acc_id_1, acc_id_2));