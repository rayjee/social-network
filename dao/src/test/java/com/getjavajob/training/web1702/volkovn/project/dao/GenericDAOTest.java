package com.getjavajob.training.web1702.volkovn.project.dao;

import org.h2.tools.RunScript;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-config.xml", "classpath:dao-test-config.xml"})
public abstract class GenericDAOTest<T> {
    private static final String INIT_SQL = "init.sql";
    private static final String DROP_SQL = "drop.sql";

    private DAO<T> dao;
    @PersistenceContext
    protected EntityManager entityManager;

    @Before
    public void setUp() throws IOException, SQLException {
        Session session = entityManager.unwrap(Session.class);
        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                try (BufferedReader reader = new BufferedReader(
                             new InputStreamReader(
                                     this.getClass().getClassLoader().getResourceAsStream(INIT_SQL)))) {
                    RunScript.execute(connection, reader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dao = getDAO();
    }

    protected abstract DAO<T> getDAO();

    @After
    public void tearDown() throws SQLException, IOException {
        Session session = entityManager.unwrap(Session.class);
        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                try (BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                this.getClass().getClassLoader().getResourceAsStream(DROP_SQL)))) {
                    RunScript.execute(connection, reader);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    @Transactional
    public void testCreate() {
        T object = getObject();
        T created = dao.create(object);
        assertEquals(object, created);
    }

    @Test
    @Transactional
    public void testUpdate() {
        T object = getObject();
        dao.create(object);
        changeObject(object);
        dao.update(object);
        entityManager.flush();
        entityManager.clear();
        assertEquals(object, dao.get(getPrimaryKey()));
    }

    protected abstract void changeObject(T object);

    @Test
    @Transactional
    public void testDelete() {
        T object = getObject();
        dao.create(object);
        entityManager.flush();
        entityManager.clear();
        dao.delete(getPrimaryKey());
        assertNull(dao.get(getPrimaryKey()));
    }

    protected abstract Object getPrimaryKey();

    @Test
    @Transactional
    public void testGetAll() {
        T object1 = getObject();
        dao.create(object1);
        T object2 = getObject();
        dao.create(object2);
        entityManager.flush();
        entityManager.clear();
        List<T> expected = asList(object1, object2);
        assertEquals(expected, dao.getAll());
    }

    protected abstract T getObject();
}
