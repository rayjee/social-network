package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.IntStream.range;
import static org.junit.Assert.*;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class AccountDAOTest extends GenericDAOTest<Account> {
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private GroupMembershipDAO groupMembershipDAO;
    @Autowired
    private PhoneDAO phoneDAO;
    private int emailCount;

    @Override
    protected DAO<Account> getDAO() {
        return accountDAO;
    }

    @Override
    protected void changeObject(Account account) {
        account.setEmail("Updated email");
    }

    @Override
    protected Object getPrimaryKey() {
        return (long) 1;
    }

    @Override
    protected Account getObject() {
        Account account = new Account();
        account.setFirstName("Ivan");
        account.setPatronymic("Ivanovich");
        account.setLastName("Ivanov");
        account.setBirthday(LocalDate.of(1900, 1, 1));
        account.setHomeAddress("Home Address");
        account.setWorkAddress("Work Address");
        account.setEmail("email" + ++emailCount  + "@gmail.com");
        account.setIcq("123456");
        account.setSkype("skype");
        account.setExtra("Some extra information added by user - silly quotes and so on");
        account.setPassword("1");
        return account;
    }

    @Test
    @Transactional
    public void testGetFriends() {
        Account account1 = getObject();
        accountDAO.create(account1);
        Account account2 = getObject();
        accountDAO.create(account2);
        Account account3 = getObject();
        accountDAO.create(account3);
        Account account4 = getObject();
        accountDAO.create(account4);
        accountDAO.insertFriendship(account1, account2);
        accountDAO.insertFriendship(account1, account3);
        assertEquals(asList(account2, account3), accountDAO.getFriends(account1.getId()));
    }

    @Test
    @Transactional
    public void testGetGroupMembers() {
        Account account1 = getObject();
        accountDAO.create(account1);
        Account account2 = getObject();
        accountDAO.create(account2);
        Account account3 = getObject();
        accountDAO.create(account3);
        Group group = new Group();
        group.setName("Group");
        group.setCreator(account1);
        groupDAO.create(group);
        GroupMembership groupMembership1 = new GroupMembership();
        groupMembership1.setAccount(account1);
        groupMembership1.setGroup(group);
        groupMembership1.setStatus(2);
        groupMembershipDAO.create(groupMembership1);
        GroupMembership groupMembership2 = new GroupMembership();
        groupMembership2.setAccount(account3);
        groupMembership2.setGroup(group);
        groupMembership2.setStatus(2);
        groupMembershipDAO.create(groupMembership2);
        assertEquals(asList(account1, account3), accountDAO.getGroupMembers(group));
    }

    @Test
    @Transactional
    public void testGetGroupRequests() {
        Account account1 = getObject();
        accountDAO.create(account1);
        Account account2 = getObject();
        accountDAO.create(account2);
        Account account3 = getObject();
        accountDAO.create(account3);
        Group group = new Group();
        group.setName("Group");
        group.setCreator(account1);
        groupDAO.create(group);
        GroupMembership groupMembership1 = new GroupMembership();
        groupMembership1.setAccount(account2);
        groupMembership1.setGroup(group);
        groupMembership1.setStatus(1);
        groupMembershipDAO.create(groupMembership1);
        GroupMembership groupMembership2 = new GroupMembership();
        groupMembership2.setAccount(account3);
        groupMembership2.setGroup(group);
        groupMembership2.setStatus(1);
        groupMembershipDAO.create(groupMembership2);
        assertEquals(asList(account2, account3), accountDAO.getGroupRequests(group));
    }

    @Test
    @Transactional
    public void testGetByEmail() {
        Account account = getObject();
        accountDAO.create(account);
        assertEquals(account, accountDAO.getByEmail("email1@gmail.com"));
    }

    @Test
    @Transactional
    public void testGetByEmailAndHash() {
        Account account = getObject();
        accountDAO.create(account);
        assertEquals(account, accountDAO.getByEmailAndHash("email1@gmail.com", "1"));
    }

    @Test
    @Transactional
    public void testGetByFirstName() {
        Account account = getObject();
        accountDAO.create(account);
        List<Account> ivan = accountDAO.getByName("Ivan", 0, 10);
        System.out.println(ivan);
        assertEquals(singletonList(account), ivan);
    }

    @Test
    @Transactional
    public void testGetByLastName() {
        Account account = getObject();
        accountDAO.create(account);
        assertEquals(singletonList(account), accountDAO.getByName("Ivanov", 0, 10));
    }

    @Test
    @Transactional
    public void testInsertFriendship() {
        Account account1 = getObject();
        Account account2 = getObject();
        accountDAO.create(account1);
        accountDAO.create(account2);
        accountDAO.insertFriendship(account1, account2);
        entityManager.clear();
        account1 = accountDAO.get((long) 1);
        account2 = accountDAO.get((long) 2);
        assertTrue(account1.getFriends().contains(account2));
        assertFalse(account2.getFriends().contains(account1));
        assertTrue(account2.getFriendOf().contains(account1));
        assertFalse(account1.getFriendOf().contains(account2));
    }

    @Test
    @Transactional
    public void testDeleteFriendship() {
        Account account1 = getObject();
        accountDAO.create(account1);
        Account account2 = getObject();
        accountDAO.create(account2);
        accountDAO.insertFriendship(account1, account2);
        accountDAO.insertFriendship(account2, account1);
        accountDAO.deleteFriendship(account1, account2);
        entityManager.clear();
        account1 = accountDAO.get((long) 1);
        account2 = accountDAO.get((long) 2);
        assertFalse(account1.getFriends().contains(account2));
        assertFalse(account2.getFriendOf().contains(account1));
        assertTrue(account2.getFriends().contains(account1));
        assertTrue(account1.getFriendOf().contains(account2));
        accountDAO.deleteFriendship(account2, account1);
        entityManager.clear();
        account1 = accountDAO.get((long) 1);
        account2 = accountDAO.get((long) 2);
        assertFalse(account2.getFriends().contains(account1));
        assertFalse(account1.getFriendOf().contains(account2));
    }

    @Test
    @Transactional
    public void testCreateWithPhones() {
        Account account = getObject();
        List<Phone> phones = getPhones(account);
        account.addPhones(phones);
        accountDAO.create(account);
        entityManager.clear();
        account = accountDAO.get((long) 1);
        assertEquals(phones, account.getPhones());
    }

    @Test
    @Transactional
    public void testDeleteWithPhones() {
        Account account = getObject();
        List<Phone> phones = getPhones(account);
        account.addPhones(phones);
        accountDAO.create(account);
        accountDAO.delete(account.getId());
        entityManager.flush();
        range(1, phones.size() + 1).forEach(index -> assertNull(phoneDAO.get((long) index)));
    }

    @Test
    @Transactional
    public void testDeleteAccountWithFriendshipsWithoutFriends() {
        Account account1 = getObject();
        accountDAO.create(account1);
        Account account2 = getObject();
        accountDAO.create(account2);
        accountDAO.insertFriendship(account1, account2);
        entityManager.getEntityManagerFactory().getCache().evict(Account.class, (long) 2);
        account2 = accountDAO.get((long) 2);
        assertTrue(account2.getFriendOf().contains(account1));
        accountDAO.delete(account1.getId());
        entityManager.flush();
        entityManager.clear();
        account2 = accountDAO.get((long) 2);
        assertNotNull(account2);
        assertFalse(account2.getFriendOf().contains(account1));
    }

    @Test
    @Transactional
    public void testDeleteWithGroupMembershipWithoutGroups() {
        Account account = getObject();
        accountDAO.create(account);
        Group group = new Group();
        group.setName("Group");
        group.setCreator(account);
        groupDAO.create(group);
        GroupMembership groupMembership = new GroupMembership();
        groupMembership.setAccount(account);
        groupMembership.setGroup(group);
        groupMembership.setStatus(2);
        groupMembershipDAO.create(groupMembership);
        entityManager.flush();
        entityManager.clear();
        account = accountDAO.get((long) 1);
        accountDAO.delete(account.getId());
        entityManager.flush();
        group = groupDAO.get((long) 1);
        assertNull(accountDAO.get((long) 1));
        assertNotNull(group);
        assertFalse(group.getMemberships().contains(groupMembership));
    }

    private List<Phone> getPhones(Account account) {
        Phone phone1 = new Phone();
        phone1.setAccount(account);
        phone1.setCountry("1");
        phone1.setArea("2");
        phone1.setNumber("3");
        phone1.setType(PhoneType.PRV);
        Phone phone2 = new Phone();
        phone2.setAccount(account);
        phone2.setCountry("3");
        phone2.setArea("2");
        phone2.setNumber("1");
        phone2.setType(PhoneType.WRK);
        return asList(phone1, phone2);
    }
}
