package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembership;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class GroupDAOTest extends GenericDAOTest<Group> {
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private GroupMembershipDAO groupMembershipDAO;

    @Override
    protected DAO<Group> getDAO() {
        return groupDAO;
    }

    @Override
    protected void changeObject(Group group) {
        group.setName("Updated name");
    }

    @Override
    protected Object getPrimaryKey() {
        return (long) 1;
    }

    @Override
    @Transactional
    protected Group getObject() {
        Group group = new Group();
        group.setName("Group name");
        group.setDescription("Description");
        group.setCreationDate(now());
        Account account = getAccount();
        accountDAO.create(account);
        group.setCreator(account);
        return group;
    }

    @Test
    @Transactional
    public void testGetGroupsByAccount() {
        Group group1 = getObject();
        groupDAO.create(group1);
        Group group2 = getObject();
        groupDAO.create(group2);
        Group group3 = getObject();
        groupDAO.create(group3);
        Account account = getAccount();
        accountDAO.create(account);
        GroupMembership groupMembership1 = new GroupMembership();
        groupMembership1.setAccount(account);
        groupMembership1.setGroup(group1);
        groupMembership1.setStatus(2);
        groupMembershipDAO.create(groupMembership1);
        GroupMembership groupMembership2 = new GroupMembership();
        groupMembership2.setAccount(account);
        groupMembership2.setGroup(group2);
        groupMembership2.setStatus(2);
        groupMembershipDAO.create(groupMembership2);
        assertEquals(asList(group1, group2), groupDAO.getGroupsByAccount(account));
    }

    private Account getAccount() {
        Account account = new Account();
        account.setFirstName("First name");
        account.setLastName("Last name");
        account.setEmail("Email");
        account.setPassword("1");
        return account;
    }

    @Test
    @Transactional
    public void testGetByName() {
        Group group1 = getObject();
        groupDAO.create(group1);
        Group group2 = getObject();
        groupDAO.create(group2);
        Group group3 = getObject();
        group3.setName("Another name");
        groupDAO.create(group3);
        assertEquals(asList(group1, group2), groupDAO.getByName("Group name", 0, 10));
    }
}
