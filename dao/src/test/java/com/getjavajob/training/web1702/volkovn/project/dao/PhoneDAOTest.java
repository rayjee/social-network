package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Phone;
import org.springframework.beans.factory.annotation.Autowired;

import static com.getjavajob.training.web1702.volkovn.project.model.PhoneType.PRV;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class PhoneDAOTest extends GenericDAOTest<Phone> {
    @Autowired
    private PhoneDAO phoneDAO;
    @Autowired
    private AccountDAO accountDAO;

    @Override
    protected DAO<Phone> getDAO() {
        return phoneDAO;
    }

    @Override
    protected void changeObject(Phone phone) {
        phone.setNumber("changed");
    }

    @Override
    protected Object getPrimaryKey() {
        return (long) 1;
    }

    @Override
    protected Phone getObject() {
        Phone phone = new Phone();
        phone.setCountry("7");
        phone.setArea("999");
        phone.setNumber("9999999");
        phone.setType(PRV);
        Account account = getAccount();
        accountDAO.create(account);
        phone.setAccount(account);
        return phone;
    }

    private Account getAccount() {
        Account account = new Account();
        account.setFirstName("First name");
        account.setLastName("Last name");
        account.setEmail("email");
        account.setPassword("password");
        return account;
    }
}
