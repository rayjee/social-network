package com.getjavajob.training.web1702.volkovn.project.dao;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembership;
import com.getjavajob.training.web1702.volkovn.project.model.GroupMembershipPK;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

public class GroupMembershipDAOTest extends GenericDAOTest<GroupMembership> {
    @Autowired
    private GroupMembershipDAO groupMembershipDAO;
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private GroupDAO groupDAO;
    private int emailCount;
    private Account account;
    private Group group;

    @Override
    protected DAO<GroupMembership> getDAO() {
        return groupMembershipDAO;
    }

    @Override
    protected void changeObject(GroupMembership groupMembership) {
        groupMembership.setStatus(2);
    }

    @Override
    protected Object getPrimaryKey() {
        GroupMembershipPK primaryKey = new GroupMembershipPK();
        primaryKey.setAccount(account);
        primaryKey.setGroup(group);
        return primaryKey;
    }

    @Override
    protected GroupMembership getObject() {
        GroupMembership groupMembership = new GroupMembership();
        account = getAccount();
        groupMembership.setAccount(account);
        group = getGroup();
        groupMembership.setGroup(group);
        groupMembership.setStatus(1);
        return groupMembership;
    }

    private Account getAccount() {
        Account account = new Account();
        account.setFirstName("Ivan");
        account.setPatronymic("Ivanovich");
        account.setLastName("Ivanov");
        account.setEmail("email" + ++emailCount  + "@gmail.com");
        account.setIcq("123456");
        account.setSkype("skype");
        account.setPassword("1");
        return account;
    }

    private Group getGroup() {
        Group group = new Group();
        group.setName("Group name");
        group.setDescription("Description");
        Account account = getAccount();
        accountDAO.create(account);
        group.setCreator(account);
        return group;
    }

    @Test
    @Transactional
    public void testGetMembershipStatus() {
        Account account1 = getAccount();
        accountDAO.create(account1);
        Account account2 = getAccount();
        accountDAO.create(account2);
        Group group = new Group();
        group.setName("Group");
        group.setCreator(account1);
        groupDAO.create(group);
        groupMembershipDAO.joinGroup(group, account1);
        assertEquals(1, groupMembershipDAO.getMembershipStatus(group, account1));
        assertEquals(0, groupMembershipDAO.getMembershipStatus(group, account2));
    }

    @Test
    @Transactional
    public void testLeaveGroup() {
        Account account1 = getAccount();
        accountDAO.create(account1);
        Account account2 = getAccount();
        accountDAO.create(account2);
        Account account3 = getAccount();
        accountDAO.create(account3);
        Group group = new Group();
        group.setName("Group");
        group.setCreator(account1);
        groupDAO.create(group);
        groupMembershipDAO.joinGroup(group, account1);
        groupMembershipDAO.joinGroup(group, account2);
        groupMembershipDAO.joinGroup(group, account3);
        groupMembershipDAO.approveMembershipRequest(group, account1);
        groupMembershipDAO.approveMembershipRequest(group, account2);
        groupMembershipDAO.approveMembershipRequest(group, account3);
        groupMembershipDAO.leaveGroup(group, account1);
        assertEquals(asList(account2, account3), accountDAO.getGroupMembers(group));
    }
}
