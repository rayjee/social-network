<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Search results</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/searchresults.css" />
</head>
<body>
<div id="searchresultsgrid">
    <%@include file="header.jsp"%>
    <div id="entityswitch">
        <div id="entityswitchgrid">
            <div>
                <input type="button" value="Account" id="account-switch">
            </div>
            <div>
                <input type="button" value="Group" id="group-switch">
            </div>
        </div>
    </div>
    <div id="results">
        <div id="account-results"></div>
        <div id="group-results"></div>
    </div>
    <div id="resultsbuttons">

        <div id="account-buttons">
            <div class="resultsbuttonsgrid">
                <div>
                    <input type="button" value="Start" id="sr-start-account-results">
                </div>
                <div>
                    <input type="button" value="Prev" id="sr-prev-account-results">
                </div>
                <div>
                    <input type="button" value="Next" id="sr-next-account-results">
                </div>
                <div>
                    <input type="button" value="End" id="sr-end-account-results">
                </div>
            </div>
        </div>
        <div id="group-buttons">
            <div class="resultsbuttonsgrid">
                <div>
                    <input type="button" value="Start" id="sr-start-group-results">
                </div>
                <div>
                    <input type="button" value="Prev" id="sr-prev-group-results">
                </div>
                <div>
                    <input type="button" value="Next" id="sr-next-group-results">
                </div>
                <div>
                    <input type="button" value="End" id="sr-end-group-results">
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    var ctx = '${pageContext.request.contextPath}';
    var limit = '${limit}';
    var groupTotal = '${groupTotal}';
    var accountTotal = '${accountTotal}';
    var searchString = '${searchString}';
</script>
<script src="${pageContext.request.contextPath}/js/search-results.js"></script>
</body>
</html>
