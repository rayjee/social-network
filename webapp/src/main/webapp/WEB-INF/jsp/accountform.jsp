<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="account" class="com.getjavajob.training.web1702.volkovn.project.model.Account" scope="request"/>
<input type="hidden" id="accountId" name="id" value="${account.id}">
<div class="param">
    <div class="paramname">First name</div>
    <div class="paramvalue">
        <input type="text" name="firstName" value="${account.firstName}">
    </div>
</div>
<div class="param">
    <div class="paramname">Last name</div>
    <div class="paramvalue">
        <input type="text" name="lastName" value="${account.lastName}">
    </div>
</div>
<div class="param">
    <div class="paramname">Patronymic</div>
    <div class="paramvalue">
        <input type="text" name="patronymic" value="${account.patronymic}">
    </div>
</div>
<div class="param">
    <input type="hidden" id="birthday" value="${birthday}">
    <div class="paramname">Birthday</div>
    <div class="paramvalue">
        <input type="text" id="datepicker" name="birthday" readonly>
    </div>
</div>
<div class="param">
    <div class="paramname">Email</div>
    <div class="paramvalue">
        <input type="email" name="email" value="${account.email}">
    </div>
</div>
<div class="param">
    <div class="paramname">Home address</div>
    <div class="paramvalue">
        <input type="text" name="homeAddress" value="${account.homeAddress}">
    </div>
</div>
<div class="param">
    <div class="paramname">Work address</div>
    <div class="paramvalue">
        <input type="text" name="workAddress" value="${account.workAddress}">
    </div>
</div>
<div id="add-phone">
    <div class="param">
        <div class="paramname">Add phone</div>
        <div class="paramvalue">
            <div class="phone-country"><input id="phonecountry" type="text" maxlength="3" ></div>
            <div class="phone-area"><input id="phonearea" type="text" maxlength="4" ></div>
            <div class="phone-number"><input id="phonenumber" type="text" maxlength="13" ></div>
            <div class="phone-type">
                <select id="phonetype">
                    <option value="Private">Private</option>
                    <option value="Work">Work</option>
                </select>
            </div>
            <div><input type="button" name="addphone" value="Add" onclick="addPhone()"></div>
        </div>
    </div>
</div>
<div id="phones">
    <c:forEach var="phone" items="${account.phones}" varStatus="status">
        <div id="phone-param${status.index}" class="param">
            <div class="paramname">${phone.type.fullType} phone</div>
            <div class="paramvalue">
                <div class="phone-country">
                    <input type="text" name="phones[${status.index}].country" readonly value="${phone.country}">
                </div>
                <div class="phone-area">
                    <input type="text" name="phones[${status.index}].area" readonly value="${phone.area}">
                </div>
                <div class="phone-number">
                    <input type="text" name="phones[${status.index}].number" readonly value="${phone.number}">
                </div>
                <input type="hidden" name="phones[${status.index}].type" value="${phone.type}">
                <input type="hidden" name="phones[${status.index}].account.id" value="${account.id}">
                <div>
                    <input type="button" value="Delete" onclick="deletePhone(${status.index})">
                </div>
            </div>
        </div>
    </c:forEach>
</div>
<input id="phones-size" type="hidden" value="${account.phones.size()}">
<div class="param">
    <div class="paramname">ICQ</div>
    <div class="paramvalue">
        <input type="text" name="icq" value="${account.icq}">
    </div>
</div>
<div class="param">
    <div class="paramname">Skype</div>
    <div class="paramvalue">
        <input type="text" name="skype" value="${account.skype}">
    </div>
</div>
<div class="param">
    <div class="paramname">Password</div>
    <div class="paramvalue"><input type="password" name="password"></div>
</div>
<div>
    <div class="paramname">Avatar</div>
    <div class="paramvalue">
        <input type="file" name="picture">
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/account-form.js"></script>
<div id="dialog"></div>