<div id="groupname" class="entityname">
    <div>
        <a href="${pageContext.request.contextPath}/group${group.id}">
            <img class="imgentity" src="${pageContext.request.contextPath}/group/image/${group.id}">
        </a>
    </div>
    <c:out value="${group.name}"/>
    <c:if test="${group.creator.equals(user)}">
        <a href="${pageContext.request.contextPath}/group${group.id}edit">
            <img class="imgedit" src="${pageContext.request.contextPath}/images/edit.png">
        </a>
    </c:if>
</div>
