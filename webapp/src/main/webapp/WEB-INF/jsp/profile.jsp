<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Profile</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/profile.css" />
</head>
<body>
<div id="profilegrid">
<%@ include file="header.jsp" %>
    <jsp:useBean id="account" class="com.getjavajob.training.web1702.volkovn.project.model.Account" scope="request"/>
    <c:if test="${account.equals(user)}">
        <div id="profilebuttons">
            <div class="a-container">
                <a href="/profile/${account.id}/toxml">
                    <div class="divbutton">
                        To XML
                    </div>
                </a>
            </div>
            <form action="/profile/${account.id}/fromxml" id="upload-xml-form" method="post"
                  enctype="multipart/form-data">
                <input type="file" class="ui-helper-hidden" id="upload-xml" name="xml">
            </form>
            <div class="a-container">
                <a id="upload-link-xml">
                    <div class="divbutton">
                        From XML
                    </div>
                </a>
            </div>
        </div>
    </c:if>
    <div id="profilename" class="entityname">
        <div><img class="imgentity" src="${pageContext.request.contextPath}/profile/image/${account.id}"></div>
        <c:out value="${account.firstName} ${account.lastName}"/>
        <c:if test="${account.equals(user)}">
            <a href="${pageContext.request.contextPath}/profile${account.id}edit">
                <img class="imgedit" src="${pageContext.request.contextPath}/images/edit.png">
            </a>
        </c:if>
    </div>
    <div class="info">
        <div class="basic">
            <div class="pname">
                <img class="imgparam" src="${pageContext.request.contextPath}/images/bday.png">
                <c:out value="${account.birthday}"/>
            </div>
            <div class="pname">
                <img class="imgparam" src="${pageContext.request.contextPath}/images/email.png">
                <c:out value="${account.email}"/>
            </div>
            <div class="pname">
                <img class="imgparam" src="${pageContext.request.contextPath}/images/icq.png">
                <c:out value="${account.icq}"/>
            </div>
            <div class="pname">
                <img class="imgparam" src="${pageContext.request.contextPath}/images/skype.png">
                <c:out value="${account.skype}"/>
            </div>
            <div class="pname">
                <img class="imgparam" src="${pageContext.request.contextPath}/images/phone.png">
                <c:set var="firstPhone" value="${true}"/>
                <c:forEach var="phone" items="${account.phones}">
                    <c:if test="${!firstPhone}">
                        <span class="phone-align">
                    </c:if>
                    <c:out value="+${phone.country} (${phone.area}) ${phone.number}"/>
                    <c:choose>
                        <c:when test="${!firstPhone}">
                            </span>
                        </c:when>
                        <c:otherwise>
                            <c:set var="firstPhone" value="${false}"/>
                        </c:otherwise>
                    </c:choose>
                    <br>
                </c:forEach>
            </div>
        </div>
        <div class="address">
            <c:if test="${account.homeAddress != null && !account.homeAddress.trim().equals(\"\")}">
                <div class="infoheader">
                    Home
                </div>
                <div class="infovalue">
                    <c:out value="${account.homeAddress}"/>
                </div>
            </c:if>
            <c:if test="${account.workAddress != null && !account.workAddress.trim().equals(\"\")}">
                <div class="infoheader">
                    Work
                </div>
                <div class="infovalue">
                    <c:out value="${account.workAddress}"/>
                </div>
            </c:if>
        </div>
    </div>
    <div class="groups">
        <c:if test="${account.memberships.size() > 0}">
            <div class="infoheader">
                Groups
            </div>
            <div class="infovalue">
                <div id="itemgrid">
                    <c:forEach var="membership" items="${account.membershipsExceptRequests}" end="4">
                        <div class="previewtabitem">
                            <a href="${pageContext.request.contextPath}/group${membership.group.id}">
                                <img src="${pageContext.request.contextPath}/group/image/${membership.group.id}"><br>
                                    ${membership.group.name}
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </c:if>
    </div>
    <c:if test="${account.friends.size() > 0}">
        <div class="friends">
            <div class="infoheader">
                Friends
            </div>
            <div class="infovalue">
            </div>
        </div>
    </c:if>
</div>
<script src="${pageContext.request.contextPath}/js/profile-xml.js"></script>
</body>
</html>