<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Create group</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css"/>
</head>
<body>
<div id="formgrid">
    <%@ include file="header.jsp" %>
    <div id="formparams">
        <form action="${pageContext.request.contextPath}/docreategroup" id="group"
                   method="post" enctype="multipart/form-data">
            <%@include file="groupform.jsp" %>
        </form>
    </div>
    <div id="formbutons">
        <div><input class="creategroup" type="submit" form="group" value="Create group"></div>
    </div>
</div>
</body>
</html>

