<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Group</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/group.css" />
</head>
<body>
<div id="groupgrid">
    <%@ include file="header.jsp"%>
    <%@ include file="groupname.jsp"%>
    <jsp:useBean id="group" class="com.getjavajob.training.web1702.volkovn.project.model.Group" scope="request"/>
    <div id="groupdescription">
        <c:out value="${group.description}"/>
    </div>
    <div id="groupbuttons">
        <jsp:useBean id="status" class="com.getjavajob.training.web1702.volkovn.project.util.MembershipStatusBean" scope="request"/>
        <c:if test="${status.status == status.none}">
            <div class="a-container">
                <a href="${pageContext.request.contextPath}/joingroup?groupId=${group.id}">
                    <div class="divbutton">
                        Join
                    </div>
                </a>
            </div>
        </c:if>
        <c:if test="${status.status == status.member || status.status == status.moderator}">
            <div class="a-container">
                <a href="${pageContext.request.contextPath}/leavegroup?groupId=${group.id}">
                    <div class="divbutton">
                        Leave
                    </div>
                </a>
            </div>
        </c:if>
        <c:if test="${status.status == status.request}">
            <div class="a-container">
                <a href="${pageContext.request.contextPath}/leavegroup?groupId=${group.id}">
                    <div class="divbutton">
                        Delete request
                    </div>
                </a>
            </div>
        </c:if>
        <c:if test="${group.creator.equals(user)}">
            <div class="a-container">
                <a href="${pageContext.request.contextPath}/deletegroup?groupId=${group.id}">
                    <div class="divbutton">
                        Delete
                    </div>
                </a>
            </div>
        </c:if>
        <c:if test="${status.status == status.moderator}">
            <div class="a-container">
                <a href="${pageContext.request.contextPath}/group${group.id}/requests">
                    <div class="divbutton">
                        See requests
                    </div>
                </a>
            </div>
        </c:if>
    </div>
    <div id="groupbody">
        <div class="infoheader">
            Members
        </div>
        <div class="infovalue">
            <div id="itemgrid">
                <c:forEach var="membership" items="${group.membershipsExceptRequests}" end="5">
                    <div class="previewtabitem">
                        <a href="${pageContext.request.contextPath}/profile${membership.account.id}">
                            <img src="${pageContext.request.contextPath}/profile/image/${membership.account.id}"><br>
                                ${membership.account.firstName}
                        </a>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</body>
</html>
