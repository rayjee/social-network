<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css"/>
</head>
<body>
<div id="formgrid">
    <%@ include file="header.jsp" %>
    <div id="formparams">
        <form action="${pageContext.request.contextPath}/doregister" id="account" method="post" enctype="multipart/form-data">
            <%@include file="accountform.jsp" %>
        </form>
    </div>
    <div id="formbutons">
        <div><input type="submit" onclick="confirm()" value="Register"></div>
    </div>
</div>
</body>
</html>
