<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error page</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/error.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css"/>
</head>
<body>
<div id="errorgrid">
    <%@include file="header.jsp"%>
    <div id="error">
        ${param.errormessage}
    </div>
</div>
</body>
</html>
