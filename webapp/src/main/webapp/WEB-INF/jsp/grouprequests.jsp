<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="group" class="com.getjavajob.training.web1702.volkovn.project.model.Group" scope="request"/>
<html>
<head>
    <title>Group requests</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/group.css" />
</head>
<body>
<div id="groupgrid">
    <%@ include file="header.jsp"%>
    <%@ include file="groupname.jsp" %>
    <div id="groupbody">
        <jsp:useBean id="accounts" class="java.util.ArrayList" scope="request"/>
        <c:forEach var="account" items="${accounts}">
            <div>
                <div class="itemlistrow">
                    <a href="${pageContext.request.contextPath}/profile${account.id}">
                        <img class="imgitemlist" src="${pageContext.request.contextPath}/profile/image/${account.id}">
                            ${account.firstName} ${account.lastName}
                    </a>
                </div>
                <div class="a-container">
                    <a href="${pageContext.request.contextPath}/approverequest?groupId=${group.id}&accountId=${account.id}">
                        <div class="divbutton">
                            Approve
                        </div>
                    </a>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>
