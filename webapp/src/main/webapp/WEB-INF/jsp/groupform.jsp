<jsp:useBean id="group" class="com.getjavajob.training.web1702.volkovn.project.model.Group" scope="request"/>
<input type="hidden" name="id" value="${group.id}">
<div class="param">
    <div class="paramname">Name</div>
    <div class="paramvalue">
        <input type="text" name="name" value="${group.name}">
    </div>
</div>
<div class="textareaparam">
    <div class="paramname">Description</div>
    <div class="paramvalue">
        <textarea name="description">${group.description}</textarea>
    </div>
</div>
<div>
    <div class="paramname">Picture</div>
    <div class="paramvalue">
        <input type="file" name="picture">
    </div>
</div>