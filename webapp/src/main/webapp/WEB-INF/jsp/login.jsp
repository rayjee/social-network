<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="icon" href="${pageContext.request.contextPath}/favicon.png">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/header.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/login.css" />
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
<div id="logingrid">
    <%@ include file="header.jsp" %>
    <c:if test="${invalidPassword}">
        <div id="invalidpassword">
            <span class="alert">Invalid login or password. Try again.</span>
        </div>
        <c:set var="invalidPassword" value="${false}" scope="session"/>
    </c:if>
    <div id="loginform">
        <form id="login" action="${pageContext.request.contextPath}/dologin" method="post">
            <div class="param">
                <div class="paramname">Login</div>
                <div class="paramvalue"><input type="text" name="login" placeholder="Email"></div>
            </div>
            <div class="param">
                <div class="paramname">Password</div>
                <div class="paramvalue"><input type="password" name="password" placeholder="Password"></div>
            </div>
        </form>
    </div>
    <div id="loginbuttons">
        <div class="loginbutton">
            <input type="submit" value="Log in" form="login">
        </div>
        <div class="registerbutton">
            <form action="${pageContext.request.contextPath}/register" method="post">
                <input type="submit" value="Register">
            </form>
        </div>
        <div class="or">
            or
        </div>
    </div>
    <div id="witness">
        <label>
            <input type="checkbox" name="witness" form="login">
            Witness me
        </label>
    </div>
</div>
</body>
</html>
