<jsp:useBean id="user" scope="session" class="com.getjavajob.training.web1702.volkovn.project.model.Account"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script src="${pageContext.request.contextPath}/jquery/external/jquery/jquery.js"></script>
<script src="${pageContext.request.contextPath}/jquery/jquery-ui.js"></script>
<header>
    <div id="headergrid">
        <div class="logo">social.network</div>
        <c:if test="${user.firstName != null}">
            <div class="search">
                <form action="${pageContext.request.contextPath}/search" method="post">
                    <input id="searchInput" type="search" name="search" placeholder="search...">
                    <button id="search"></button>
                    <a href="${pageContext.request.contextPath}/creategroup">
                        <span class="create-group">New group</span>
                    </a>
                </form>
            </div>
            <script>var ctx = '${pageContext.request.contextPath}';</script>
            <script src="${pageContext.request.contextPath}/js/search.js"></script>
        </c:if>
        <c:if test="${user.firstName != null}">
            <div class="username">
                <a href="${pageContext.request.contextPath}/profile${user.id}">
                    <c:out value="${user.firstName} ${user.lastName}"/>
                </a>
                <br>
                <a href="${pageContext.request.contextPath}/logout">
                    <span class="logout">Logout</span>
                </a>
            </div>
        </c:if>
    </div>
</header>
