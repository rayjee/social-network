var total;
var currentEntity;

function init() {
    $('#sr-start-account-results').click(function () {
        getSearchResultPage(0);
    });
    $('#sr-prev-account-results').click(function () {
        getSearchResultPage(0);
    });
    $('#sr-start-group-results').click(function () {
        getSearchResultPage(0);
    });
    $('#sr-prev-group-results').click(function () {
        getSearchResultPage(0);
    });
    $('#sr-end-account-results').click(function () {
        var remainder = accountTotal % limit;
        if (remainder === 0) {
            remainder = limit;
        }
        var endOffset = accountTotal - remainder;
        getSearchResultPage(endOffset);
    });
    $('#sr-next-account-results').click(function () {
        getSearchResultPage(limit);
    });
    $('#sr-end-group-results').click(function () {
        var remainder = groupTotal % limit;
        if (remainder === 0) {
            remainder = limit;
        }
        var endOffset = groupTotal - remainder;
        getSearchResultPage(endOffset);
    });
    $('#sr-next-group-results').click(function () {
        getSearchResultPage(limit);
    });
    $('#account-switch').click(function () {
        switchClick('account');
    });
    $('#group-switch').click(function () {
        switchClick('group');
    });
    getSearchResultPage(0);
}

$(function(){
    currentEntity = 'account';
    total = accountTotal;
    $('#group-buttons').addClass("ui-helper-hidden");
    $('#group-results').addClass("ui-helper-hidden");
    init();
});

function getSearchResultPage(offset) {
    $.ajax( {
        url : ctx + '/pagesearch',
        data : {
            offset : offset,
            name : searchString,
            entity : currentEntity
        },
        success : function (data) {
            setSearchButtons();
            printResults();

            function setSearchButtons() {
                var srPrev = $('#sr-prev-' + currentEntity + '-results');
                var srStart = $('#sr-start-' + currentEntity + '-results');
                srPrev.unbind().click(function() {
                    getSearchResultPage(offset - limit)
                });
                var hiddenClass = "ui-helper-hidden";
                if (offset - limit < 0) {
                    srPrev.addClass(hiddenClass);
                    srStart.addClass(hiddenClass);
                } else {
                    if (srPrev.hasClass(hiddenClass)) {
                        srPrev.removeClass(hiddenClass);
                        srStart.removeClass(hiddenClass);
                    }
                }
                var srNext = $('#sr-next-' + currentEntity + '-results');
                var srEnd = $('#sr-end-' + currentEntity + '-results');
                var nextOffset = parseInt(offset) + parseInt(limit);
                srNext.unbind().click(function() {
                    getSearchResultPage(nextOffset)
                });
                if (nextOffset >= total) {
                    console.log(nextOffset);
                    console.log(total);
                    srNext.addClass(hiddenClass);
                    srEnd.addClass(hiddenClass);
                } else {
                    if (srNext.hasClass(hiddenClass)) {
                        srNext.removeClass(hiddenClass);
                        srEnd.removeClass(hiddenClass);
                    }
                }
            }

            function printResults() {
                $('.itemlistrow').remove();
                var results = $('#' + currentEntity + '-results');
                var i;
                for (i = 0; i < data.length; i++) {
                    var itemListRow = $('<div/>', {
                        class: "itemlistrow"
                    });
                    itemListRow.appendTo(results);
                    var a = $('<a/>', {
                        href: data[i].id,
                        text: data[i].value
                    });
                    a.appendTo(itemListRow);
                    $('<img/>', {
                        class: "imgitemlist",
                        src: data[i].icon
                    }).appendTo(a);
                }
            }
        }
    })
}

function switchClick(entity) {
    var hiddenClass = "ui-helper-hidden";
    var accountResults = $('#account-results');
    var groupResults = $('#group-results');
    var accountButtons = $('#account-buttons');
    var groupButtons = $('#group-buttons');
    if (entity === 'account') {
        total = accountTotal;
        currentEntity = 'account';
        if (accountResults.hasClass(hiddenClass)) {
            accountButtons.removeClass(hiddenClass);
            accountResults.removeClass(hiddenClass);
        }
        groupResults.addClass(hiddenClass);
        groupButtons.addClass(hiddenClass);
    }
    if (entity === 'group') {
        total = groupTotal;
        currentEntity = 'group';
        groupResults.removeClass(hiddenClass);
        groupButtons.removeClass(hiddenClass);
        accountResults.addClass(hiddenClass);
        accountButtons.addClass(hiddenClass);
    }
    init();
}