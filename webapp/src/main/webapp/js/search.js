$( function() {
    $( "#searchInput" ).autocomplete({
        source: function( request, response ) {
            $.ajax( {
                url: ctx + '/partsearch',
                data: {
                    name: request.term
                },
                success: function( data ) {
                    response(data);
                }
            } );
        },
         minLength: 2,
        select: function( event, ui ) {
            window.location.replace(ui.item.url);
        }
    } ).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $('<li class="ui-menu-item-with-icon"></li>')
            .data("item.autocomplete", item)
            .append('<div><a><img class="item-icon" src="' + item.icon + '"/>' + item.label + '</a></div>')
            .appendTo(ul);
    };
} );