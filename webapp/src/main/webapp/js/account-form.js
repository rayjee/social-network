$(function () {
    var datePicker = $("#datepicker");
    datePicker.datepicker({
        dateFormat: "dd.mm.yy"
    });
    datePicker.datepicker("setDate", $("#birthday").val());
    var phoneTypeSelect = $("#phonetype");
    phoneTypeSelect.selectmenu({
        width: 100
    });
    phoneTypeSelect.data("ui-selectmenu").button.addClass("phonetype-select");
    $(document).ready(function () {
        function addValidation(inputId) {
            var phoneCountry = $(inputId);
            phoneCountry.keydown(validateInput);
            phoneCountry.bind('paste', null, validatePaste);
        }

        addValidation("#phonecountry");
        addValidation("#phonearea");
        addValidation("#phonenumber");
    });
});

function validatePaste() {
    var element = $(event.target);
    var oldVal = $(element).val();
    setTimeout(function () {
        var text = $(element).val();
        if (!text.match("^[0-9]+$")) {
            $(element).val(oldVal);
        }
    }, 0);
}

function validateInput(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

var inc = (function () {
    var counter = parseInt(document.getElementById("phones-size").value) - 1;
    return function (increment) {
        return counter += increment;
    }
})();

function deletePhone(phoneNum) {
    $("#phone-param" + phoneNum).remove();
}

function getPhoneTypeEnumValue(phType) {
    switch (phType) {
        case "Private": return "PRV";
        case "Work": return "WRK";
    }
}

function addPhoneToForm(phCountry, phArea, phNumber, phType) {
    var phoneNum = inc(1);
    var param = $('<div/>', {
        class: "param",
        id: "phone-param" + phoneNum
    });
    param.appendTo($("#phones"));
    var paramName = $('<div/>', {
        class: "paramname",
        text: phType + " phone"
    });
    paramName.appendTo(param);
    var paramValue = $('<div/>', {
        class: "paramvalue"
    });
    paramValue.appendTo(param);
    appendPhonePart("country", phCountry);
    appendPhonePart("area", phArea);
    appendPhonePart("number", phNumber);

    $('<input/>', {
        type: "hidden",
        name: "phones[" + phoneNum + "].type",
        value: getPhoneTypeEnumValue(phType)
    }).appendTo(paramValue);
    $('<input/>', {
        type: "hidden",
        name: "phones[" + phoneNum + "].account.id",
        value: $("#accountId").val()
    }).appendTo(paramValue);

    var deleteButtonDiv = $('<div/>');
    deleteButtonDiv.appendTo(paramValue);
    $('<input/>', {
        type: "button",
        value: "Delete",
        click: function() {
            deletePhone(phoneNum);
        }
    }).appendTo(deleteButtonDiv);

    function appendPhonePart(name, value) {
        var div = $('<div/>', {
            class: "phone-" + name
        });
        div.appendTo(paramValue);
        $('<input/>', {
            type: "text",
            readOnly: true,
            name: "phones[" + phoneNum + "]." + name,
            value: value
        }).appendTo(div);
    }
}

function addPhone() {
    var phCountryElement = $("#phonecountry");
    var phAreaElement = $("#phonearea");
    var phNumberElement = $("#phonenumber");
    var emptyFieldAlert = $("#empty-field-alert");
    if (phCountryElement.val() !== '' && phAreaElement.val() !== '' && phNumberElement.val() !== '') {
        var phType = $("#phonetype").find("option:selected").text();
        addPhoneToForm(phCountryElement.val(), phAreaElement.val(), phNumberElement.val(), phType);
        phCountryElement.val("");
        phAreaElement.val("");
        phNumberElement.val("");
        emptyFieldAlert.remove();
    } else {
        if (emptyFieldAlert.length === 0) {
            var param = $('<div/>', {
                class: "param",
                id: "empty-field-alert"
            });
            param.appendTo($("#add-phone"));
            var paramName = $('<div/>', {
                class: "paramname"
            });
            paramName.appendTo(param);
            var paramValue = $('<div/>', {
                class: "paramvalue"
            });
            paramValue.appendTo(param);
            $('<span/>', {
                class: "alert",
                text: "Empty field!"
            }).appendTo(paramValue);
        }
    }
}

function confirm() {
    var form = $("#dialog").dialog({
        title: "Are you sure?",
        height: 110,
        width: "auto"
    });
    form.html("<div id='buttongrid'><div><input type='submit' value='Yes' form='account'></div>" +
        "<div><input id='no' type='submit' value='No'></div></div>");
    form.dialog("widget").find("#no").on("click", function () {
        form.dialog("close");
    });
    form.dialog("widget").find("input").css({"font-weight": "400", "font-size": "0.9em", "width": "auto"});
    return false;
}