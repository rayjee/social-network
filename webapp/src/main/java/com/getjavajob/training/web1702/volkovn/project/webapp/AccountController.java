package com.getjavajob.training.web1702.volkovn.project.webapp;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.service.ServiceException;
import com.getjavajob.training.web1702.volkovn.project.util.PasswordException;
import com.getjavajob.training.web1702.volkovn.project.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

import static com.getjavajob.training.web1702.volkovn.project.util.PasswordUtils.generatePasswordHash;

@Controller
@Logging
public class AccountController {

    private static final String PROFILE_START_URI = "/profile";

    @Autowired
    private AccountService accountService;
    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value = "/profile{id}edit", method = RequestMethod.GET)
    public ModelAndView showEdit(@PathVariable("id") long id) {
        Account account = accountService.getWithPhones(id);
        ModelAndView modelAndView = new ModelAndView("profileedit");
        modelAndView.addObject("account", account);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        if (account.getBirthday() != null) {
            modelAndView.addObject("birthday", account.getBirthday().format(formatter));
        }
        return modelAndView;
    }

    @RequestMapping(value = "/profile{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView showAccount(@PathVariable("id") long id) {
        Account account = accountService.getFullyInitialized(id);
        return new ModelAndView("profile", "account", account);
    }

    @RequestMapping(value = "/profileedit", method = RequestMethod.POST)
    public ModelAndView updateAccount(Account account, HttpSession session) throws PasswordException {
        account.getPhones().removeIf(phone -> phone.getNumber() == null);
        account.setPassword(generatePasswordHash(account.getPassword()));
        if (account.getPicture().length == 0) {
            Account oldAccount = accountService.get(account.getId());
            account.setPicture(oldAccount.getPicture());
        }
        accountService.update(account);
        session.setAttribute("user", account);
        String contextPath = servletContext.getContextPath();
        return new ModelAndView("redirect:" + contextPath + PROFILE_START_URI + account.getId());
    }

    @RequestMapping(value = "/doregister", method = RequestMethod.POST)
    public ModelAndView createAccount(Account account, HttpSession session) throws PasswordException {
        account.getPhones().removeIf(phone -> phone.getNumber() == null);
        account.getPhones().forEach(phone -> phone.setAccount(account));
        account.setPassword(generatePasswordHash(account.getPassword()));
        Account createdAccount = accountService.create(account);
        session.setAttribute("user", createdAccount);
        String contextPath = servletContext.getContextPath();
        return new ModelAndView("redirect:" + contextPath + PROFILE_START_URI + createdAccount.getId());
    }

    @RequestMapping(value = "/profile/{id}/toxml", method = RequestMethod.GET)
    public HttpEntity<byte[]> exportToXml(@PathVariable long id) throws ServiceException {
        String xml = accountService.toXml(id);
        byte[] documentBody = xml.getBytes();
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "xml"));
        header.setContentLength(documentBody.length);
        header.setContentDispositionFormData("attachment", "account" + id + ".xml");
        return new HttpEntity<>(documentBody, header);
    }

    @RequestMapping(value = "/profile/{id}/fromxml", method = RequestMethod.POST)
    public ModelAndView importFromXml(@PathVariable long id, @RequestPart MultipartFile xml)
            throws IOException, ServiceException {
        accountService.fromXml(id, xml.getBytes());
        return showAccount(id);
    }
}
