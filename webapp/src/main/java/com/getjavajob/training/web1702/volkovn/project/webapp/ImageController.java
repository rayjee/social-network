package com.getjavajob.training.web1702.volkovn.project.webapp;

import com.getjavajob.training.web1702.volkovn.project.model.Picture;
import com.getjavajob.training.web1702.volkovn.project.service.AccountService;
import com.getjavajob.training.web1702.volkovn.project.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedInputStream;
import java.io.IOException;

import static java.lang.Thread.currentThread;

@RestController
@Logging
public class ImageController {
    private static final String DEFAULT_PROFILE_PICTURE = "profile.png";
    private static final String DEFAULT_GROUP_PICTURE = "group.png";

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/profile/image/{id}", method = RequestMethod.GET)
    public byte[] getAccountImage(@PathVariable long id) throws IOException {
        return getImage(accountService.get(id), DEFAULT_PROFILE_PICTURE);
    }

    @RequestMapping(value = "/group/image/{id}", method = RequestMethod.GET)
    public byte[] getGroupImage(@PathVariable long id) throws IOException {
        return getImage(groupService.get(id), DEFAULT_GROUP_PICTURE);
    }

    private byte[] getImage(Picture object, String fileName) throws IOException {
        byte[] picture = object.getPicture();
        if (picture != null && picture.length > 0) {
            return picture;
        } else {
            return getDefaultPicture(fileName);
        }
    }

    private byte[] getDefaultPicture(String fileName) throws IOException {
        try (BufferedInputStream input = new BufferedInputStream(currentThread()
                .getContextClassLoader()
                .getResourceAsStream(fileName))) {
            byte[] defaultPicture = new byte[input.available()];
            input.read(defaultPicture);
            return defaultPicture;
        }
    }
}
