package com.getjavajob.training.web1702.volkovn.project.webapp;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

@Aspect
public class LoggerAspect {
    private static final Logger logger = LoggerFactory.getLogger(LoggerAspect.class);

    @Around("execution(* com.getjavajob.training.web1702.volkovn.project.webapp.*.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Method method = ((MethodSignature) point.getSignature()).getMethod();
        String methodName = method.getName();
        Class<?> type = point.getTarget().getClass();
        String className = type.getSimpleName();
        boolean logging = isLoggingMethod(method, type);
        if (logging) {
            logger.info("Entering method {} of class {} with args {}", methodName, className, point.getArgs());
        }
        Object result = point.proceed();
        if (logging) {
            logger.info("Exiting method {} of class {}", methodName, className);
        }
        return result;
    }

    private boolean isLoggingMethod(Method method, Class<?> type) {
        return method.isAnnotationPresent(Logging.class) || type.isAnnotationPresent(Logging.class);
    }
}
