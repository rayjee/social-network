package com.getjavajob.training.web1702.volkovn.project.webapp.dto;

public class LoginReqParam {
    private String login;
    private String password;
    private String witness;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWitness() {
        return witness;
    }

    public void setWitness(String witness) {
        this.witness = witness;
    }

    @Override
    public String toString() {
        return "LoginReqParam{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", witness='" + witness + '\'' +
                '}';
    }
}
