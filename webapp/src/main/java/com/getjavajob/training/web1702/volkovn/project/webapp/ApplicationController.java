package com.getjavajob.training.web1702.volkovn.project.webapp;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.service.AccountService;
import com.getjavajob.training.web1702.volkovn.project.service.GroupService;
import com.getjavajob.training.web1702.volkovn.project.service.ServiceException;
import com.getjavajob.training.web1702.volkovn.project.webapp.dto.LoginReqParam;
import com.getjavajob.training.web1702.volkovn.project.webapp.dto.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.web1702.volkovn.project.service.AccountService.INVALID_PASSWORD_MESSAGE;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

@Controller
@SessionAttributes("invalidPassword")
@Logging
public class ApplicationController {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
    private static final int SEARCH_PAGE_LIMIT = 2;

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value = "/dologin", method = RequestMethod.POST)
    public ModelAndView login(LoginReqParam param, HttpSession session) {
        String contextPath = servletContext.getContextPath();
        ModelAndView modelAndView;
        if (param.getLogin() != null && param.getPassword() != null) {
            try {
                Account account = accountService.validateAccount(param.getLogin(), param.getPassword());
                modelAndView = new ModelAndView("redirect:" + contextPath + "/profile" + account.getId());
                session.setAttribute("user", account);
                if (param.getWitness() != null) {
                    modelAndView.addObject("login", new Cookie("login", account.getEmail()));
                    modelAndView.addObject("password", new Cookie("password", account.getPassword()));
                    logger.info("Cookies are created for user {}", param.getLogin());
                }
                logger.info("Successful login");
                return modelAndView;
            } catch (ServiceException e) {
                modelAndView = new ModelAndView("redirect:" + contextPath + "/");
                if (INVALID_PASSWORD_MESSAGE.equals(e.getMessage())) {
                    modelAndView.addObject("invalidPassword", true);
                    logger.warn("Invalid password for user {}", param.getLogin());
                } else {
                    logger.error("Failed to log in", e);
                }
            }
        } else {
            modelAndView = new ModelAndView("redirect:" + contextPath + "/");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpSession session) {
        session.invalidate();
        logger.info("Session is invalidated");
        String contextPath = servletContext.getContextPath();
        ModelAndView modelAndView = new ModelAndView("redirect:" + contextPath + "/login");
        deleteCookie(modelAndView, "login");
        deleteCookie(modelAndView, "password");
        logger.info("Cookies are deleted");
        return modelAndView;
    }

    private void deleteCookie(ModelAndView modelAndView, String name) {
        Cookie cookie = new Cookie(name, "");
        cookie.setMaxAge(0);
        modelAndView.addObject(name, cookie);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView search(@RequestParam String search) {
        String contextPath = servletContext.getContextPath();
        ModelAndView modelAndView = new ModelAndView("redirect:" + contextPath + "/searchresults", "searchString", search);
        modelAndView.addObject("limit", SEARCH_PAGE_LIMIT);
        modelAndView.addObject("accountTotal", accountService.getCountByName(search));
        modelAndView.addObject("groupTotal", groupService.getCountByName(search));
        return modelAndView;
    }

    @RequestMapping(value = "/searchresults", method = RequestMethod.GET)
    public ModelAndView showSearchResults
            (@RequestParam String searchString, @RequestParam int limit, @RequestParam int accountTotal, @RequestParam int groupTotal) {
        ModelAndView modelAndView = new ModelAndView("searchresults", "searchString", searchString);
        modelAndView.addObject("limit", limit);
        modelAndView.addObject("accountTotal", accountTotal);
        modelAndView.addObject("groupTotal", groupTotal);
        return modelAndView;
    }

    @RequestMapping(value = "/pagesearch", method = RequestMethod.GET)
    @ResponseBody
    public List<SearchResult> getSearchResultsByPage
            (@RequestParam int offset, @RequestParam String name, @RequestParam String entity) {
        String contextPath = servletContext.getContextPath();
        if ("account".equals(entity)) {
            List<Account> accounts = accountService.getByName(name, offset, SEARCH_PAGE_LIMIT);
            return getAccountSearchResults(accounts, contextPath);
        }
        if ("group".equals(entity)) {
            List<Group> groups = groupService.getByName(name, offset, SEARCH_PAGE_LIMIT);
            return getGroupSearchResults(contextPath, groups);
        }
        throw new IllegalArgumentException("Invalid entity");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView showRegister() {
        return new ModelAndView("register");
    }

    @RequestMapping(value = {"/login", "/"}, method = RequestMethod.GET)
    public ModelAndView showLogin() {
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/partsearch", method = RequestMethod.GET)
    @ResponseBody
    public List<SearchResult> searchByPartName(@RequestParam String name) {
        List<Account> accounts = accountService.getAll();
        accounts.removeIf(account -> !containsIgnoreCase(account.getFirstName() + ' ' + account.getLastName(), name));
        String contextPath = servletContext.getContextPath();
        List<SearchResult> searchResults = getAccountSearchResults(accounts, contextPath);
        List<Group> groups = groupService.getAll();
        groups.removeIf(group -> !containsIgnoreCase(group.getName(), name));
        searchResults.addAll(getGroupSearchResults(contextPath, groups));
        return searchResults;
    }

    private List<SearchResult> getGroupSearchResults(String contextPath, List<Group> groups) {
        List<SearchResult> searchResults = new ArrayList<>();
        groups.forEach(group -> {
            String url = contextPath + "/group" + group.getId();
            String icon = contextPath + "/group/image/" + group.getId();
            searchResults.add(new SearchResult(url, group.getName(), icon));
        });
        return searchResults;
    }

    private List<SearchResult> getAccountSearchResults(List<Account> accounts, String contextPath) {
        List<SearchResult> searchResults = new ArrayList<>();
        accounts.forEach(account -> {
            String url = contextPath + "/profile" + account.getId();
            String accountName = account.getFirstName() + ' ' + account.getLastName();
            String icon = contextPath + "/profile/image/" + account.getId();
            searchResults.add(new SearchResult(url, accountName, icon));
        });
        return searchResults;
    }
}
