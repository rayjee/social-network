package com.getjavajob.training.web1702.volkovn.project.webapp;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.model.Group;
import com.getjavajob.training.web1702.volkovn.project.util.MembershipStatus;
import com.getjavajob.training.web1702.volkovn.project.util.MembershipStatusBean;
import com.getjavajob.training.web1702.volkovn.project.service.AccountService;
import com.getjavajob.training.web1702.volkovn.project.service.GroupService;
import com.getjavajob.training.web1702.volkovn.project.service.GroupMembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.util.List;

import static java.time.LocalDate.now;

@Controller
@SessionAttributes("user")
@Logging
public class GroupController {
    private static final String GROUP_START_URI = "/group";

    @Autowired
    private GroupService groupService;
    @Autowired
    private GroupMembershipService groupMembershipService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private ServletContext servletContext;

    @RequestMapping(value = "/deletegroup", method = RequestMethod.GET)
    public String deleteGroup(@RequestParam long groupId) {
        groupService.delete(groupId);
        String contextPath = servletContext.getContextPath();
        return "redirect:" + contextPath + "/";
    }

    @RequestMapping(value = "/group{id}edit", method = RequestMethod.GET)
    public ModelAndView showEdit(@PathVariable long id) {
        Group group = groupService.get(id);
        return new ModelAndView("groupedit", "group", group);
    }

    @RequestMapping(value = "/joingroup", method = RequestMethod.GET)
    public String joinGroup(@RequestParam long groupId, @ModelAttribute("user") Account user) {
        groupMembershipService.joinGroup(groupId, user.getId());
        String contextPath = servletContext.getContextPath();
        return "redirect:" + contextPath + GROUP_START_URI + groupId;
    }

    @RequestMapping(value = "/leavegroup", method = RequestMethod.GET)
    public String leaveGroup(@RequestParam long groupId, @ModelAttribute("user") Account user) {
        groupMembershipService.leaveGroup(groupId, user.getId());
        String contextPath = servletContext.getContextPath();
        return "redirect:" + contextPath + GROUP_START_URI + groupId;
    }

    @RequestMapping(value = "/group{id}/requests", method = RequestMethod.GET)
    public ModelAndView showRequests(@PathVariable long id) {
        Group group = groupService.getFullyInitialized(id);
        ModelAndView modelAndView = new ModelAndView("grouprequests");
        modelAndView.addObject("group", group);
        List<Account> requestingAccounts = accountService.getGroupRequests(group);
        modelAndView.addObject("accounts", requestingAccounts);
        return modelAndView;
    }

    @RequestMapping(value = "/group{groupId}", method = RequestMethod.GET)
    public ModelAndView showGroup(@PathVariable long groupId, @ModelAttribute("user") Account user) {
        Group group = groupService.getFullyInitialized(groupId);
        ModelAndView modelAndView = new ModelAndView("group", "group", group);
        MembershipStatus status = groupMembershipService.getMembershipStatus(group, user);
        MembershipStatusBean statusBean = new MembershipStatusBean();
        statusBean.setStatus(status);
        modelAndView.addObject("status", statusBean);
        return modelAndView;
    }

    @RequestMapping(value = "/docreategroup", method = RequestMethod.POST)
    public String createGroup(Group group, HttpSession session) {
        group.setCreationDate(now());
        Account user = (Account) session.getAttribute("user");
        group.setCreator(user);
        Group createdGroup = groupService.create(group, user);
        String contextPath = servletContext.getContextPath();
        return "redirect:" + contextPath + GROUP_START_URI + createdGroup.getId();
    }

    @RequestMapping(value = "/dogroupedit", method = RequestMethod.POST)
    public String updateGroup(Group group) {
        if (group.getPicture().length == 0) {
            Group oldGroup = groupService.get(group.getId());
            group.setPicture(oldGroup.getPicture());
        }
        groupService.update(group);
        String contextPath = servletContext.getContextPath();
        return "redirect:" + contextPath + GROUP_START_URI + group.getId();
    }

    @RequestMapping(value = "/approverequest", method = RequestMethod.GET)
    public String approveRequest(@RequestParam long accountId, @RequestParam long groupId) {
        groupMembershipService.approveMembershipRequest(groupId, accountId);
        String contextPath = servletContext.getContextPath();
        return "redirect:" + contextPath + GROUP_START_URI + groupId + "/requests";
    }

    @RequestMapping(value = "/creategroup", method = RequestMethod.GET)
    public ModelAndView showCreate() {
        return new ModelAndView("creategroup");
    }
}
