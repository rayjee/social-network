package com.getjavajob.training.web1702.volkovn.project.webapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(CookieInterceptor.class);

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            modelAndView.getModel()
                    .values()
                    .stream()
                    .filter(value -> value instanceof Cookie)
                    .forEach(value -> {
                Cookie cookie = (Cookie) value;
                response.addCookie(cookie);
                logger.info("Cookie \"{}\" is added to response", cookie.getName());
            });
        }
    }
}
