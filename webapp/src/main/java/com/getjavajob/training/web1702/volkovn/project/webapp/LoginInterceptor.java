package com.getjavajob.training.web1702.volkovn.project.webapp;

import com.getjavajob.training.web1702.volkovn.project.model.Account;
import com.getjavajob.training.web1702.volkovn.project.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginInterceptor implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

    @Autowired
    private AccountService accountService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        String contextPath = request.getContextPath();
        HttpSession session = request.getSession();
        Account user = (Account) session.getAttribute("user");
        if ((user == null || user.getFirstName() == null)  && !isWebappResource(requestURI, contextPath)) {
            logger.debug("Attempt to authorize by cookies");
            user = authorizeByCookies(request);
            if (user == null) {
                logger.debug("Unsuccessful authorization by cookies");
                if (isRedirectToLogin(requestURI, contextPath)) {
                    response.sendRedirect(contextPath + "/login");
                    return false;
                } else {
                    return true;
                }
            } else {
                logger.debug("Successful authorization by cookies for user {}", user.getEmail());
            }
        }
        if (isRedirectToLogin(requestURI, contextPath) || isWebappResource(requestURI, contextPath)) {
            return true;
        } else {
            response.sendRedirect(contextPath + "/profile" + user.getId());
            return false;
        }
    }

    private boolean isWebappResource(String requestURI, String contextPath) {
        return requestURI.startsWith(contextPath + "/css/") || requestURI.startsWith(contextPath + "/js/") ||
                requestURI.startsWith(contextPath + "/images/") || requestURI.startsWith(contextPath + "/jquery/");
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    private boolean isRedirectToLogin(String requestURI, String contextPath) {
        return !(contextPath + "/").equals(requestURI) && !(contextPath + "/login").equals(requestURI) &&
                !(contextPath + "/dologin").equals(requestURI) &&
                !(contextPath + "/register").equals(requestURI) && !(contextPath + "/doregister").equals(requestURI);
    }

    private Account authorizeByCookies(HttpServletRequest request) throws IOException {
        Cookie[] cookies = request.getCookies();
        String login = null;
        String password = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie != null) {
                    switch (cookie.getName()) {
                        case "login":
                            login = cookie.getValue();
                            break;
                        case "password":
                            password = cookie.getValue();
                            break;
                    }
                }
            }
        } else {
            logger.debug("No cookies are present");
        }
        if (login != null && password != null) {
            Account account = accountService.getByEmailAndHash(login, password);
            if (account != null) {
                HttpSession session = request.getSession();
                session.setAttribute("user", account);
                return account;
            }
        } else {
            logger.debug("No login or password cookie");
        }
        return null;
    }
}
